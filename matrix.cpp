#include <cstdint>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include "matrix.hpp"

using namespace std;

/* Nous allons stocker des matrices dans des tablaux à 1 dimension:
Par exemple, matrice de taille n x m est stockué comme:
	A_tab = [A_11, A_12, ... A_1m, A_21, ... A_2m, ... , A_1n, ..., A_nm]

Par conséquant, un élement A_ij aurait quelle indice dans le tableau A_tab ? */

/* Memory allocation for a matrix of size n x m and initilization to 0  */
double* allocateMatrix(uint64_t n, uint64_t m) {
    double* A;
    A = (double*) calloc(n* m, sizeof(double));
    return A;
}

/* Frees the memory allocated to matrix A */
void freeMatrix(double* A) {
    free(A);
}

/* Allocates a n sized vector and initializes all entries to 0 */
double* allocateVector(uint64_t n) {
    double* v;
    v = (double*) calloc(n, sizeof(double));
    return v;
}

/* Trees the memory allocated to a vector */
void freeVector(double* v) {
    free(v);
}

/* Sets a n * m matrix A to all zeros */
void setMatrixZero(double* A, uint64_t n, uint64_t m) {
    uint64_t i, j;

    for (i = 0 ; i < n ; i++) {
        for (j = 0 ; j < m ; j++) {
            /* Note that for a n x m matrix flattened to a 1D array, 
            element A_ij has index i * m + j */
            A[i* m + j] = 0.0;
        }
    }
}

/* Sets a n * n matrix A to identity */
void setMatrixIdentity(double* A, uint64_t n) {
    uint64_t i, j;

    for (i = 0 ; i < n ; i++) {
        for (j = 0 ; j < n ; j++) {
            A[i* n + j] = 0.0;
        }

        A[i* n + i] = 1.0;
    }
}

/* Copies a matrix  */
void copyMatrix(double* B, double* A, uint64_t n, uint64_t m) {
    uint64_t i, j;

    for (i = 0 ; i < n ; i++) {
        for (j = 0 ; j < m ; j++) {
            B[i* m + j] = A[i* m + j];
        }
    }
}

/* Writes a matrix to a stream. For example, writing a matrix to standard output is
writeMatrix(stdout, A, n, m); A sream can also be a file. */
void writeMatrix(FILE *stream, double *A, uint64_t n, uint64_t m) {
	cout << "Size : " << n << " x " << m << endl;
    int i, j;
    for (i = 0 ; i < n ; ++i) {
        for (j = 0 ; j < m ; ++j) {
            fprintf(stream, "%f \t", A[i * m + j]);
        }

        fprintf(stream, "\n");
    }
}

// The function computes the element-by-element abs of matrix A
void absMatrix(double* Aabs, double* A, uint64_t n, uint64_t m) {
    uint64_t i, j;
    for (i = 0 ; i < n ; ++i) {
        for (j = 0 ; j < m ; ++j) {
            Aabs[i* m + j] = fabs(Aabs[i* m + j]);
        }
    }
}

/* Performs addition of two matrix A (size n x m) and B (size n x m).
The result S = A + B is a n x m matrix. We consider that S is allocated outside the function. */
void matrixAdd(double* S, double* A, double* B, uint64_t n, uint64_t m) {
    uint64_t i, j;
    for (i = 0 ; i < n ; ++i) {
        for (j = 0 ; j < m ; ++j) {
            S[i* m + j] = A[i* m + j] + B[i* m + j];
        }
    }
}

/* Performs subtraction of two matrix A (size n x m) and B (size n x m).
The result S = A - B is a n x m matrix. We consider that S is allocated outside the function. */
void matrixSub(double* S, double* A, double* B, uint64_t n, uint64_t m) {
    uint64_t i, j;
    for (i = 0 ; i < n ; ++i) {
        for (j = 0 ; j < m ; ++j) {
            S[i* m + j] = A[i* m + j] - B[i* m + j];
        }
    }
}

/* For a double m x n matrix A the function returns its maximum in absolute value
element. */
double getMaxInMatrix(double max, double* A, uint64_t n, uint64_t m) {
    double maxA = fabs(A[0]);
    double current = fabs(A[0]);
    int i, j;

    for (i = 0 ; i < n ; ++i) {
        for (j = 0 ; j < m ; ++j) {
            current = fabs(A[i* m + j]);
            if (current > maxA) {
                maxA = current;
            }
        }
    }
    
    return maxA;
}

/**
 * @brief Get the Matrix Value object
 * 
 * @param A matrix
 * @param i index column
 * @param j index line
 * @param n number of columns
 * @return double value at index 
 */
double matrixValue(double* A, uint64_t i, uint64_t j, uint64_t n) {
	return A[((i - 1) * n + j) - 1];
}

/**
 * @brief Get the Matrix Index object
 * 
 * @param A matrix
 * @param i index column
 * @param j index line
 * @param n number of columns
 * @return double value at index 
 */
int matrixIndex(double* A, uint64_t i, uint64_t j, uint64_t n) {
	return ((i - 1) * n + j) - 1;
}

void insertArrayInMatrix(double* A, double tab[], uint64_t size) {
    for (int i = 0 ; i < size ; i++) {
        A[i] = tab[i];
    }
}

/* Rajouter les prototypes de vos méthodes ici. Par exemple */

/* Performs naive multiplication of matrix A (size p x k) by a matrix B (size k x r).
The result matrix S = A*B  is of size (p x r).
We assume that S has already been allocated outside the function. */
void matrixMultiplyNaive(double * S, double * A, double * B, uint64_t p, uint64_t k, uint64_t r) {
    for (int a = 0 ; a < p * r; a++) {
        S[a] = 0;
        double l[k];
        double h[k];

        for (int i = 0 ; i < k ; i++) {
            //l[i] = A[a/p*p + i];
            l[i] = matrixValue(A, a/r+1, i+1, k);
            cout << l[i] << " ";
        }

        for (int i = 0 ; i < k ; i++) {
            //h[i] = B[i * r + a % r];
            h[i] = matrixValue(B, i+1, a%r+1, r);
            cout << h[i] << " ";
        }

        for(int i = 0 ; i < k ; i++) {
            S[a] += l[i] * h[i];
        }
    }
}

void matrixMultiplyStrassen(double *S, double *A, double *B, uint64_t n) { 
    uint64_t mid_n = n / 2;

    double *A11 = allocateMatrix(mid_n, mid_n);
    double *A12 = allocateMatrix(mid_n, mid_n);
    double *A21 = allocateMatrix(mid_n, mid_n);
    double *A22 = allocateMatrix(mid_n, mid_n);
	double *B11 = allocateMatrix(mid_n, mid_n);
    double *B12 = allocateMatrix(mid_n, mid_n);
    double *B21 = allocateMatrix(mid_n, mid_n);
    double *B22 = allocateMatrix(mid_n, mid_n);
	
	double *prxy1 = allocateMatrix(mid_n, mid_n);
	double *prxy2 = allocateMatrix(mid_n, mid_n);

	double *M1 = allocateMatrix(mid_n, mid_n);
	double *M2 = allocateMatrix(mid_n, mid_n);
	double *M3 = allocateMatrix(mid_n, mid_n);
	double *M4 = allocateMatrix(mid_n, mid_n);
	double *M5 = allocateMatrix(mid_n, mid_n);
	double *M6 = allocateMatrix(mid_n, mid_n);
	double *M7 = allocateMatrix(mid_n, mid_n);

	double *C0 = allocateMatrix(mid_n, mid_n);
	double *C1 = allocateMatrix(mid_n, mid_n);
	double *C2 = allocateMatrix(mid_n, mid_n);
	double *C3 = allocateMatrix(mid_n, mid_n);

	if (n == 2) { // Ordre 2
		matrixMultiplyNaive(S, A, B, 2, 2 ,2);
	} else {
		for (int i = 0 ; i < n / 2 ; i++) {
			for (int j = 0 ; j < n / 2 ; j++) {
				A11[i * mid_n + j] = A[i * n + j];
				A12[i * mid_n + j] = A[i * n + (j + mid_n)];
				A21[i * mid_n + j] = A[(i + mid_n) * n + j];
				A22[i * mid_n + j] = A[(i + mid_n) * n + (j + mid_n)];

				B11[i * mid_n + j] = B[i * n + j];
				B12[i * mid_n + j] = B[i * n + (j + mid_n)];
				B21[i * mid_n + j] = B[(i + mid_n) * n + j];
				B22[i * mid_n + j] = B[(i + mid_n) * n + (j + mid_n)];
			}
		}

        // M1 = (A0 + A3) * (B0 + B3)
        matrixAdd(prxy1, A11, A22, mid_n, mid_n);
        matrixAdd(prxy2, B11, B22, mid_n, mid_n);
        matrixMultiplyStrassen(M1, prxy1, prxy2, mid_n);

        // M2 = (A2 + A3) * B0
        matrixAdd(prxy1, A21, A22, mid_n, mid_n);
        matrixMultiplyStrassen(M2, prxy1, B11, mid_n);

        // M3 = A0 * (B1 - B3)
        matrixSub(prxy1, B12, B22, mid_n, mid_n);
        matrixMultiplyStrassen(M3, A11, prxy1, mid_n);

        // M4 = A3 * (B2 - B0)
        matrixSub(prxy1, B21, B11, mid_n, mid_n);
        matrixMultiplyStrassen(M4, A22, prxy1, mid_n);

        // M5 = (A0 + A1) * B3
        matrixAdd(prxy1, A11, A12, mid_n, mid_n);
        matrixMultiplyStrassen(M5, prxy1, B22, mid_n);

        // M6 = (A2 - A0) * (B0 + B1)
        matrixSub(prxy1, A21, A11, mid_n, mid_n);
        matrixAdd(prxy2, B11, B12, mid_n, mid_n);
        matrixMultiplyStrassen(M6, prxy1, prxy2, mid_n);

        // M7 = (A1 - A3) * (B2 + B3)
        matrixSub(prxy1, A12, A22, mid_n, mid_n);
        matrixAdd(prxy2, B21, B22, mid_n, mid_n);
        matrixMultiplyStrassen(M7, prxy1, prxy2, mid_n);

		// C0 = M1 + M4 - M5 + M7
		matrixAdd(prxy1, M1, M4, mid_n, mid_n);
		matrixSub(prxy2, prxy1, M5, mid_n, mid_n);
		matrixAdd(C0, prxy2, M7, mid_n, mid_n);

		// C1 = M3 + M5
		matrixAdd(C1, M3, M5, mid_n, mid_n);

		// C2 = M2 + M4
		matrixAdd(C2, M2, M4, mid_n, mid_n);

		// C3 = M1 - M2 + M3 + M6
		matrixSub(prxy1, M1, M2, mid_n, mid_n);
		matrixAdd(prxy2, M3, M6, mid_n, mid_n);
		matrixAdd(C3, prxy1, prxy2, mid_n, mid_n);

		for (int i = 0 ; i < mid_n ; i++) {
			for (int j = 0 ; j < mid_n ; j++) {
				S[i * n + j] = C0[i * mid_n + j];
				S[i * n + (j + mid_n)] = C1[i * mid_n + j];
				S[(i + mid_n) * n + j] = C2[i * mid_n + j];
				S[(i + mid_n) * n + (j + mid_n)] = C3[i * mid_n + j];
			}
		}
	}
}

/**
 * @brief Sustract l1 to l2 with a multiply factor
 * 
 * @param A matrix
 * @param l number of lines
 * @param c number of columns
 * @param l1 index of A matrix line
 * @param l2 index of B matrix line
 * @param f multiply factor
 * @return void
 */
void soustractLines(double * A, uint64_t l, uint64_t c, uint64_t l1, uint64_t l2, double f) {
	for (int i = 0 ; i < c ; i++) {
        A[(l2-1)*c + i] -= A[(l1-1)*c + i]*f;
    }
}

/**
 * @brief Divide a line
 * 
 * @param A matrix
 * @param l number of lines
 * @param c number of columns
 * @param l1 index of A matrix line
 * @param f multiply factor
 * @return void
 */
void divideLine(double * A, uint64_t l, uint64_t c, uint64_t l1, double f) {
	for (int i = 0 ; i < c ; i++) {
        A[(l1-1)*c + i] = A[(l1-1)*c + i] / f;
    }
}

/* Solves a system of linear equations Ax=b for a double-precision matrix A (size n x n). 
Uses iterative ascension algorithm. 
After the procedure, x contains the solution of Ax=b. 
We assume that x has been allocated outside the function. */
void SolveTriangularSystemUP(double* x, double* A, double* b, uint64_t n) {
    cout << " -- SolveTriangularSystemUP -- " << endl;
    double div;
    double coef;
    for(int i = n-1; i >= 0; i--) {
        div =  A[i*n + i];
        divideLine(A, n, n, i+1, div);
        divideLine(b, n, 1, i+1, div);

        for(int j = i-1; j >= 0; j--) {
            coef = A[j*n+i];
            soustractLines(A, n, n, i+1, j+1, coef);
            soustractLines(b, n, 1, i+1, j+1, coef);
        }
    }
    copyMatrix(x, b, n, 1);
}

/* Performs Gauss elimination for given a matrix A (size n x n) and a vector b (size n). Modifies directly matrix A and vector b. 
In the end of the procedure, A is upper truangular and b is modified accordingly. 
Returns a boolean variable: 
    *  true in case of success and 
    *  false in case of failure, for example matrix is impossible to triangularize. */
bool Triangularize(double* A, double* b, uint64_t n) {
    cout << " -- Triangularize -- " << endl;

    for(int i = 1; i < n; i++) {
        for(int p = 1; p < n; p++) {
            if(matrixValue(A, p, p, n) == 0) {
                return false;
            }
        }

        for(int j = i+1; j <= n; j++) {
            double fac =  1 / A[(i-1)*n + i-1] * A[(j-1)*n + i-1];

            soustractLines(A, n, n, i, j, fac);
            soustractLines(b, n, 1, i, j, fac);
        }
    }
    return true;
}

/* Solves a system of linear equations Ax=b, given a matrix A (size n x n) and vector b(size n). 
Uses Gauss elimination algorithm based on truangularization and the ascension solving. 
After the procedure, vector x contains the solution to Ax=b. 
We assume that x has been allocated outside the function. Returns a boolean variable: 
    *  true in case of success and 
    *  false in case of failure, for example matrix is of rank <n . */
bool SolveSystemGauss(double* x, double* A, double* b, uint64_t n) {
    cout << " -- SolveSystemGauss -- " << endl;
    if(Triangularize(A, b, n)) {
        SolveTriangularSystemUP(x, A, b, n);
    }
    else {
        cout << "! ERROR, TRIANGULARIZE STOPED: can't divide by 0 !" << endl;
        return false;
    }
    return true;
}

bool decompLU(double* A, uint64_t n) {
    cout << " -- decompositionLU -- " << endl;

    for(int i = 1; i < n; i++) {
        for(int p = 1; p < n; p++) {
            if(matrixValue(A, p, p, n) == 0) {
                return false;
            }
        }

        for(int j = i+1; j <= n; j++) {
            double fac =  1 / A[(i-1)*n + i-1] * A[(j-1)*n + i-1];

            //soustractLines(A, n, n, i, j, fac);

            for(int k = i; k <= n; k++) {
                cout << "LA POS " << matrixIndex(A, j, k, n) << " ET " << matrixIndex(A, i, k, n) << " fac " << fac <<  endl;
                cout << A[matrixIndex(A, j, k, n)] << " et " << A[matrixIndex(A, i, k, n)] << endl;
                A[matrixIndex(A, j, k, n)] -= fac * A[matrixIndex(A, i, k, n)];
                //cout << A[matrixIndex(A, j, k, n)] << endl;
            }

            A[matrixIndex(A, j, i, n)] = fac;
            cout << A[matrixIndex(A, j, i, n)] << endl;
            
            //A[matrixIndex(A, j, i, n)] = abs(fac);
            cout << " -*-*-*-*-*-*-*-*-*-*-*- C'EST " << abs(fac) << endl;
            writeMatrix(stdout, A, n, n);
        }
    }

    return true;
}

double det(double *A, uint64_t n) {
	double det = 1;

	for (int i = 0 ; i < n ; i++) {
		det *= A[i * n + i];
	}

	return det;
}

void splitLU(double *A, double *L, double *U, uint64_t n) {
    for (int i = 0 ; i < n ; i++) {
        for (int j = 0 ; j < n ; j++) {
            L[i * n + j] =  A[i * n + j];
            U[i * n + j] =  A[i * n + j];

            if (i > j) {
                U[i * n + j] = 0;
            } else if (i < j) {
                L[i * n + j] = 0;
            } else {
                L[i * n + j] = 1;
            }
        }
    }
}