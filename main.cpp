#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include "matrix.hpp"
#include "matrix.cpp"

using namespace std;

int main(int argc, char **argv) {
	double *x = allocateMatrix(4, 1);
	double *L = allocateMatrix(4, 4);
    double *U = allocateMatrix(4, 4);

	double *A = allocateMatrix(4, 4);
	double matrixA[16] = {10, 7, 8, 7, 7, 5, 6, 5, 8, 6, 10, 9, 7, 5, 9, 10};
	insertArrayInMatrix(A, matrixA, 16);

	cout << "Matrix A :" << endl;
	writeMatrix(stdout, A, 4, 4);
	
	decompLU(A, 4);

	cout << "Matrix LU :" << endl;
	writeMatrix(stdout, A, 4, 4);

	splitLU(A, L, U, 4);

	cout << "Matrix L :" << endl;
	writeMatrix(stdout, L, 4, 4);

	cout << "Matrix U :" << endl;
	writeMatrix(stdout, U, 4, 4);

	double *b1 = allocateMatrix(4, 1);
	double matrixB1[4] = {32, 23, 33, 31};
	insertArrayInMatrix(b1, matrixB1, 4);

	double *b2 = allocateMatrix(4, 1);
	double matrixB2[4] = {32, 23, 33, 31};
	insertArrayInMatrix(b2, matrixB2, 4);

	Triangularize(L, b1, 4);	
	SolveTriangularSystemUP(x, U, b1, 4);

	cout << "Resolved system Ax = b1 :" << endl;
	writeMatrix(stdout, x, 4, 1);
	
	return 0;
}